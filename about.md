---
layout: page
title: About me 
header-img: "img/about-bg.jpg"

---

Yo nací en una ribera del Arauca vibrador
soy hermano de la espuma,
de las garzas, de las rosas
y del sol.

Ingeniero en Informatica, Egresado de la Universidad Centrooccidental Lisandro Alvarado.

He programado en: Turbo Pascal, C, Assembler, Dbase, Visual Fox Pro, Visual Basic, Centura Team Developer (SQL Windows), PL/SQL, PHP, JS.

He desarrollado proyectos en PHP, sistemas internos de control, aplicaciones de integracion de sistemas, reportes gerenciales, reportes avanzados que extraen data de multiples sistemas y utilidades para simplificacion de tareas rutinarias, por la naturaleza de las empresas donde trabajo no puedo ofrecer muestras o pantallas/screen de mi trabajo.

En el año 2010 Realice un curso de programación avanzada en ABAP.

He trabajado con bases de datos Oracle, MS SQL Server, Mysql

Desde el año 2009, trabajo con Joomla y he decidido migrarlas a HTML5.

Uso Git por defecto en todos los proyectos que trabajo. 

Trabajo con linux, especificamente Ubuntu y no me gusta repetir tareas, casi siempre termino creando un programa que las resuelve.

Trabajo con Ubuntu desde la version 7.04, actualmente 14.04 64Btis.

Aprendiendo NodeJS, AngularJS.
