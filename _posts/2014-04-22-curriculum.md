---
layout: post
title: "Cursos 2014"
date: 2014-04-22 14:40
comments: false
categories: 
---

[AngularJS — Code School](https://www.codeschool.com/courses/shaping-up-with-angular-js)

[JavaScript — Codecademy](http://www.codecademy.com/en/tracks/javascript)

[YouTube API — Codecademy](http://www.codecademy.com/en/tracks/youtube)

[Try Git — Code School](https://www.codeschool.com/courses/try-git)

[How Create a Udemy Course — Udemy](https://www.udemy.com/official-udemy-instructor-course/)
