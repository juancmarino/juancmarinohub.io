---
layout: post
title: "Tecnologias Usadas en 2014."
date: 2015-01-08 10:16:14 -0430
comments: true
categories: 
---


* PHP 5.3 OOP
* MySQL normalised database.
* Microsoft SQL Server 2008
* Bootstrap  v3
* JQuery

Sistemas Operativos.

* Ubuntu Server 12.04, 14.04.
* Windows 8
* Windows Server 2008 RS
* Windows Server 2003

Herramientas.

* Chrome - Google
* Firefox
* Sublime Text 2


