#Pagina Personal de Juan Carlos Mariño Ardila

Blog personal, primera version usando octopress, migrado a [Jekyllrb] (http://jekyllrb.com/)

Repositorio de enlaces sociales y cursos realizados.

Prueba de uso de [GitHub Pages](https://pages.github.com/).

Basada en el tema [Clean Blog] (http://startbootstrap.com/template-overviews/clean-blog/) de  [Start Bootstrap](http://startbootstrap.com/).

Gracias a [Riley Hilliard](http://rileyh.com/) por [ReportCard.js] (http://reportcard.rileyh.com/)

