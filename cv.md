---
layout: page
title: Curriculum
header-img: "img/about-bg.jpg"

---

###Conocimientos

Desarrollo Web en PHP, HTML, CSS, Javascript, MySQL, Joomla, Wordpress, 
Prestashop, Moodle, Git, GitHub.

SAP/ ABAP (Academia), Visual Basic, Visual FoxPro, Oracle, Microsoft SQL Server, 
Centura SQL Windows, Oracle PL/SQL.

Administración y configuración de servidores Linux.

Manejo de modelos de datos, desarrollo de interfaces para la integración de sistemas; 
Optimización de código; Manejo y control de versiones de productos en desarrollo.

Diseño completo de sitios web eficaces bajo estándares, partiendo del diseño hasta el 
maquetado en HTML + CSS y programación en PHP,  AJAX, Javascript, XML, desarrollo 
de tiendas online y el desarrollo y personalización de portales a medida.

###Formación

Agosto 2011 Gestión de la Cadena de Suministros. Valencia, Estado Carabobo.

Agosto 2010 Academia Herramientas de Programación (ABAP) en Ambiente SAP. Valencia, Estado Carabobo.

2000-2002 Maestría  en Ingeniería de Sistemas,  mención Sistemas de Información 
(Escolaridad completa) Universidad Simón Bolívar, Caracas, Estado Miranda. 

1991-1998 Ingeniero en Informática, Universidad Centroccidental “Lisandro Alvarado”, Barquisimeto, Estado Lara. 

###Experiencia Profesional

####2009 - Presente Desarrollador Web en Policlínica San Javier del Arca

Desarollador web (PHP,MySQL,JS),  administrador de la plataforma web.
Instalación y configuración de plataforma e-learning Moodle. 
Administración y configuración de servidores Linux, Apache, Subversion, SSL.
Cursos Microsoft Word avanzado, Joomla basico, Moodle basico.
Elaboración reportes, consultas web, tableros de control (Admisiones ejecutadas, 
Admisiones por facturar, porcentajes de ocupación). 

####2011-2012 Suministros Hospitalarios Sonoven, C.A.

Administrador de la empresa, Implementación del Sistema Administrativo Profit 
Plus. Encargado de las gestiones de compra, venta, inventario, caja, banco, cuentas por 
pagar, cuentas por cobrar. 

####2007-2009 Coordinador Calidad y Atención al Usuario, Fondo Nacional de Ciencia, Tecnología e Innovación. (FONACIT)

Encargado del área de calidad y atención al usuario, estadísticas de gestión. Con personal a cargo (4) y funciones de desarrollador.

####2004-2007 Consultor de Sistemas, Fondo Nacional de Ciencia, Tecnología e Innovación. (FONACIT)

Desarrollo y mantenimiento de los módulos del sistema Nirvana 
(Financiero) y el Sistema Brahman (Control de Gestión). Soporte de la gestiones de
la institución. Manejo de requerimientos y nuevas funcionalidades.

Gestiones: 

* Contabilidad
* Ejecución Presupuestaria
* Cuentas por Pagar
* Ordenación de Pagos 
* Compras y Almacén 
* Gestión Operativa del Fonacit. 


####2002-2004 Especialista, Movilnet

Administración del Sistema de Hospitalización Cirugía y Maternidad de 
CANTV. Soporte a usuarios, Administración de usuarios; Manejo de nuevas 
funcionalidades y corrección de errores; Implantación de los sistemas SIPAP 
(Póliza de Automóviles) y SIICA (Control Activos).

####2001-2004 Analista Senior, Sistemas Multiplexor (SMX)

Desarrollo y mantenimiento del Sistema de Hospitalización Cirugía y 
Maternidad de CANTV. Soporte a usuarios, instalaciones, corrección de fallas, 
administración de código fuente, administración de usuarios.
Análisis, diseño y desarrollo de interfaces con el sistema SAP/R3.

####1998-2001 Analista Consultor C3, Sistemas Edmasoft

Desarrollo y mantenimiento del Sistema Brahman (Sistema para 
automatización de la gestión operativa del Consejo de Nacional de Ciencia y 
Tecnología); Optimización de los procesos, evolución, control de versiones y 
administración de servidores de desarrollo (Windows NT Server 4.0).
Programación avanzada en Centura SQL Windows.

####1995-1998 Preparador del Centro de Computación de la Universidad Centrooccidental Lisandro Alvarado.

Asesoría a los alumnos de la institución, facilitador en cursos a los 
estudiantes y al personal administrativo de la universidad.
Facilitador de los siguientes cursos:

* Visual Java
* Windows NT Workstation 4.0
* Microsoft Excel 97
* Visual Basic 5.0 
* Microsoft Access 97
* Internet
* Windows 95
* Visual Fox Pro 5.0
* Introduccion al Windows-Word 6.0.